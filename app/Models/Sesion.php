<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Usuario;

class Sesion extends Model
{
    protected $table = 'sesiones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'usuario_id',
        'expired_at',
    ];

    /**
     * Permite hacer la relacion de la sesion con el usuario
     */
    public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }
}
