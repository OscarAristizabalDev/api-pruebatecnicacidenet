<?php

namespace App\Http\Middleware;

use App\Models\Sesion;
use Closure;
use Illuminate\Http\Request;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //se obtiene el  token enviado
        $token = $request->header('Authorization');
        //se valida que en la cabecera venga un token
        if(is_null($token)){
            return $this->requestUnauthorized();
        }
        // Se consulta la informacion de la sesion a la que pertenece el token
        $sesion = $this->getSession($token);
        // si no existe una sesion relacionada al token devolvemos solicitud sin autorización
        if(is_null($sesion)){
            return $this->requestUnauthorized();
        }
        // verificamos si la sesion sigue activa
        if(!$this->isSesionActive($sesion)){
            return $this->requestUnauthorized();
        }
        // Se prolonga el tiempo de sesion del usuario
        $this->prolongateSesion($sesion);
        // Se permite hacer la operación
        return  $next($request);
    }
    /**
     * metodo que carga la sesion
     * @param type $token
     */
    private function getSession($token) {
        // Se consulta la sesion
        $sesion = Sesion::where('token', $token)->first();
        // Se retorna la sesion
        return $sesion;
    }
    /**
    * permite verificar si la sesion sigue activa
    * 
    * @param type $sesion
    * @return boolean
    */
   private function isSesionActive($sesion): bool {

        $fechaActual = strtotime(date("d-m-Y H:i:00",time()));
        $fechaSesion = strtotime($sesion->expired_at,time());
        // Se compara la fecha actual con la fecha de la sesion
        if ($fechaActual > $fechaSesion) {
            return false;             
        }
        return true;
   }

    /**
    * permite extender la sesion otros 15 minutos
    * @param type $sesion
    */
    private function prolongateSesion($sesion){
        
        $sesion->expired_at = date("Y-m-d H:i:s"  ,strtotime('+ 15 minute'));
        $sesion->save();
    
    }
   /**
    * permite obtener un response indicamndo que la solicitud require de un usuario autenticado
    * @return type
    */
   private function requestUnauthorized(){
       
        // Se devuelve la respuesta
        return response()
            ->json([
                'mensaje' => 'Debe tener autorización para usar el servicio',
                'estado' => 401
            ], 200);
    }
}
