<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Database\QueryException;

class UsuarioController extends Controller
{
    /**
     * Permite listar los usuarios registrados en estado activo
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // Se consultar los usuarios activos
            $usuarios = Usuario::where('active',true)
            ->get();

            // Se devuelve los usuarios activos
            return response()
                ->json([
                    'usuarios' => $usuarios,
                    'estado' => 200
                ], 200);  

        // Se capturan las excepciones
        } catch (QueryException $th) {
            // Se registra el error presentado en el log
            Log::error($th);
            // Se devuelve la respuesta
            return response()->json([
                'error' => 'Error al consultar los usuarios',
                'estado' => 400
            ], 200);  
        }
        
    }

    /**
     * Permite registrar un nuevo usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        try {
            // Se obtienen los valores del usuario
            $data = $request->all();
            // Se busca el usuario por email
            $tmpUsuario = Usuario::where('email',$data['email'])->first(); 
            // Se valida si hay un usuario registrado
            if(!is_null($tmpUsuario)){
                return response()
                    ->json([
                        'mensaje' => 'Ya existe un usuario registrado con email '.$tmpUsuario->email,
                        'estado' => 400
                    ], 200);
            }
            // Se registre el usuario
            $usuario = Usuario::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'age' => $data['age'],
                'gender' => $data['gender'],
                'password' => Hash::make($data['password'])
            ]);
            // Se organiza la respuesta
            $dataResponse = [
                'mensaje' => 'El usuario ha sido registrado',
                'usuario_id' => $usuario->id,
                'estado' => 200
            ];
            // Se devuelve la respuesta
            return response()
                ->json($dataResponse, 200);
        // Se capturan las excepciones
        } catch (QueryException $th) {
            // Se registra el error presentado en el log
            Log::error($th);
            // Se devuelve la respuesta
            return response()->json([
                'mensaje' => 'Error al registrar el usuario',
                'estado' => 404
            ], 200);  
        }
        
    }
    /**
     * Permite consultar un usuario por id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            // Se busca el usuario por medio del id
            $usuario = Usuario::where('id',$id)
                ->where('active',true)
                ->firstOrFail();
            // Se devuelve la respuesta
            return response()
                ->json([
                    'usuario' => $usuario, 
                    'estado' => 200
                ],200);
        // Se captura las excepciones
        } catch (ModelNotFoundException  $th) {
            // Se registra el error presentado en el log
            Log::error("Error al consultar el usuario desde la funcion show");
            // Se devuelve la respuesta
            return response()->json([
                'mensaje' => 'Usuario no encontrado',
                'estado' => 404
            ], 200);
        
        }
    }
    /**
     * Permite actualizar un usuario especifico
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            // Se busca el usuario
            $usuario = Usuario::where('id',$id)
                ->where('active',true)
                ->firstOrFail();
                
            // Se asigna los nuevos valores del usuario
            $usuario->name = $request->name;
            $usuario->email = $request->email;
            $usuario->age = $request->age;
            $usuario->gender = $request->gender;
            //$usuario->password = Hash::make($request->password);
            // Se actualiza la información del usuario
            $usuario->save();
            // Se devuelve la respuesta
            return response()
                ->json([
                    'mensaje' => 'El usuario ha sido actualizado correctamente',
                    'usuario' => $usuario, 
                    'estado' => 200
                ],200);
        
        }
        // Se capturan las excepciones
        catch (QueryException $th){
            // Se registra el error presentado en el log
            Log::error($th);
            // Se devuelve la respuesta 
            return response()->json([
                'mensaje' => 'Ya existe un usuario registrado con el email '.$request->email,
                'estado' => 400
            ], 200);  
        }
        catch (ModelNotFoundException  $th) {
            // Se registra el error presentado en el log
            Log::error($th);
            // Se devuelve la respuesta 
            return response()->json([
                'mensaje' => 'Usuario no encontrado',
                'estado' => 404
            ], 200);   
        }
    }

    /**
     * Permite eliminar un registro, que en realidad solo se cambia el estado a false
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // Se busca el usuario
            $usuario = Usuario::where('id',$id)
                ->where('active',true)
                ->firstOrFail();
            // Se actualiza el campo alive del usuario a 0, es cual indica que es un registro inexistente
            $usuario->active = false;
            $usuario->save();
            // Se devuelve la respuesta
            return response()
                ->json([
                    'usuario' => $usuario, 
                    'estado' => 200,
                    'mensaje' => 'Se ha eliminado el usuario con email '.$usuario->email
                ],200);
        // Se capturan las excepciones
        } catch (ModelNotFoundException  $th) {
            // Se registra el error presentado en el log
            Log::error("Error al consultar el usuario desde la funcion destroy");
            // Se devuelve la respuesta
            return response()->json([
                'mensaje' => 'Usuario no encontrado',
                'estado' => 404
            ], 200);   
        }
    }
}