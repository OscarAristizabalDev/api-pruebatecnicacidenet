<?php

namespace App\Http\Controllers\Auth;

use App\Models\Usuario;
use App\Models\Sesion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException; 

use DateTime;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){}
    /**
     * Autenticación de usuario
     *
     * @return void
     */
    public function login(Request $request){

        try {
            // Se obtiene las credencales
            $data = $request->all();
            // Se consulta el usuario por medio del email
            $tmpUser = Usuario::where('email', $data['email'])->firstOrFail();
            // variable para validar si tiene datos de acceso correctos
            $isCredentialsCorrect = true;
            // Se verifica si la contraseña ingresada coincide con la de la base de datos
            if(!password_verify($data['password'], $tmpUser->password)){
                $isCredentialsCorrect = false;
            }
            // En caso de no tener la contraseña correcta
            if(!$isCredentialsCorrect){
                return response()->json([
                    'mensaje' => 'Contraseña incorrecta',
                    'estado' => 400
                ], 200);        
            }
            // verificamos si el usuario fue logeado
            if($isCredentialsCorrect){
                
                $dateTime = new \DateTime();		
                // se buscan la sesion que en el momento se encuentra activa
                $sesiones = Sesion::where('usuario_id', $tmpUser->id)
                ->where('expired_at','>=', $dateTime)->get();
                // si el mismo usuario vuelve a iniciar sesion
                // se cierran las sesiones que anteriormente estaba activas
                // recorremos las sesiones encontradas
                foreach ($sesiones as $tmpSesion) {
                    // para cerrar la sesión indicamos la nueva fecha de vencimiento de la sesion
                    $tmpSesion->expired_at =  date("Y-m-d H:i:s"  ,strtotime('-1 minute'));
                    // Se actualizan las sesiones 
                    $tmpSesion->save();
                }
                //se crea un objeto sesion
                $newSession = new Sesion();
                //Se genera el token
                $newSession->token = bin2hex(openssl_random_pseudo_bytes(16));
                //se asinga el id del usuario
                $newSession->usuario_id = $tmpUser['id'];
                // Se calcula el tiempo para expirar la sesion, 
                // en este caso damos 15 minutos por temas de seguridad
                $newSession->expired_at =  date("Y-m-d H:i:s"  ,strtotime('+15 minutes'));
                // se guarda la sesion
                $newSession->save();
                // se organiza la respuesta
                $dataResponse = [
                    "token" => $newSession->token,
                    "usuario" => $tmpUser,
                    "estado" => "200"
                ];
                // Se devuelve la respuesta
                return response()
                    ->json($dataResponse, 200);
            }

        } catch (ModelNotFoundException  $th) {
            // Se registra el error presentado en el log
            Log::error("Error en inicio de sesion");
            // Se devuelve la respuesta
            return response()->json([
                'mensaje' => 'El usuario '.$data['email'].' no existe',
                'status' => 400
            ], 200); 
        }
	}
    /**
     * Permite cerrar sesión del usuario
     */
    public function logout(Request $request){
        try {
            // se recibe el token enviado
            $token = $request->header('Authorization');
            // Se busca la sesión que tenga el token recibido
            $sesion = Sesion::where('token', $token)->firstOrFail();
            $sesion->expired_at = date('Y-m-d H:i:s');
            $sesion->save();
            // Se devuelve la respuesta
            return response()
                ->json([
                    'mensaje' => 'Se ha cerrado la sesión correctamente',
                    'estado' => 200
                ], 200); 
        // Se capturan las excepciones
        } catch (\Throwable $th) {
            // Se registra el error presentado en el log
            Log::error("Error en cierre de sesion");
        }
    }
}
