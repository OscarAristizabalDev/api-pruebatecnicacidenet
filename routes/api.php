<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Ruta para inicio de sesión
Route::post('/auth/login', 'Auth\AuthController@login');
// Se aplica middleware para la administración de usuarios y cierre de sesión
Route::middleware(['user_accessible'])->group(function(){
    // Ruta para cierre de sesión del usuario
    Route::post('/auth/logout', 'Auth\AuthController@logout');
    // Ruta para consultas de usuarios
    Route::get('/usuarios', 'UsuarioController@index');
    // Ruta para consulta de usuarios por ID
    Route::get('/usuarios/detail/{id}', 'UsuarioController@show');
    // Ruta para creación de usuarios
    Route::post('/usuarios/create', 'UsuarioController@create');
    // Ruta para actualización de usuarios
    Route::put('/usuarios/update/{id}', 'UsuarioController@update');
    // Ruta para eliminación de usuarios
    Route::delete('/usuarios/delete/{id}', 'UsuarioController@destroy');
});
