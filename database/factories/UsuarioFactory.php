<?php

namespace Database\Factories;

use App\Models\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class UsuarioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Usuario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $gender = $this->faker->randomElement(['Masculino', 'Femenino']);

        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail,
            'age' => rand(1,30),
            'gender' => $gender,
            'password' => Hash::make('oscar'),
            'active' => true,
        ];
    }
}
